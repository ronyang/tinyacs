package vertx2.model;

/**
 * Project:  cwmp (aka CCFG)
 *
 * CCFG Event Source Enums
 *
 * @author: ronyang
 */
public enum EventSourceEnum {
    System,
    Autonomous,
    Operator
}
